package com.rencredit.jschool.boruak.taskmanager;


import com.rencredit.jschool.boruak.taskmanager.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
