package com.rencredit.jschool.boruak.taskmanager.api.controller;

public interface IAuthController {

    void registration();

    void logIn();

    void logOut();

}
