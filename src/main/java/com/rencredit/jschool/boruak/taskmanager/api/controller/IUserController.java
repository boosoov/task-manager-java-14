package com.rencredit.jschool.boruak.taskmanager.api.controller;

public interface IUserController {

    void updatePassword();

    void viewProfile();

    void editProfile();

}
