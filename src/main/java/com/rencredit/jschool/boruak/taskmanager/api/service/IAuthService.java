package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumeration.Role;

public interface IAuthService {

    String getUserId();

    void logIn(String login, String password);

    User registration(String login, String password);

    User registration(String login, String password, String email);

    User registration(String login, String password, Role role);

    void logOut();

}
