package com.rencredit.jschool.boruak.taskmanager.constant;

public interface ArgumentConst {

    String HELP = "-h";

    String VERSION = "-v";

    String ABOUT = "-a";

    String EXIT = "-e";

    String INFO = "-i";

    String COMMANDS = "-cmd";

    String ARGUMENTS = "-arg";

}
