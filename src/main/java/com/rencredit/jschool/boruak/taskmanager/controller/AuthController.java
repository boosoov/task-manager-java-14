package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.api.controller.IAuthController;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    public AuthController(final IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void logIn() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        authService.logIn(login, password);
        System.out.println("Login success");
    }

    @Override
    public void registration() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        final User user = authService.registration(login, password);
        System.out.println(user);
    }

    @Override
    public void logOut() {
        authService.logOut();
        System.out.println("Have been logout");
    }

}
