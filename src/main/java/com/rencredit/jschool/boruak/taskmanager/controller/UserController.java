package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.api.controller.IUserController;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

public class UserController implements IUserController {

    private final IUserService userService;

    private final IAuthService authService;

    public UserController(final IUserService userService,
                          final IAuthService authService
    ) {
        this.userService = userService;
        this.authService = authService;
    }

    @Override
    public void updatePassword() {
        System.out.println("Enter new password");
        final String password = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final User user = userService.updatePasswordById(userId, password);
        System.out.println(user);
    }

    @Override
    public void viewProfile() {
        final String userId = authService.getUserId();
        final User user = userService.getById(userId);
        System.out.println(user);
    }

    @Override
    public void editProfile() {
        System.out.println("Enter email");
        final String email = TerminalUtil.nextLine();
        System.out.println("Enter first name");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        final String middleName = TerminalUtil.nextLine();

        final String userId = authService.getUserId();
        final User user = userService.editProfileById(userId, email, firstName, lastName, middleName);
        System.out.println(user);
    }

}
