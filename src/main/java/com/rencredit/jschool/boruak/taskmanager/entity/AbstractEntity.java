package com.rencredit.jschool.boruak.taskmanager.entity;

import java.util.UUID;

public class AbstractEntity {

    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.id;
    }

}
