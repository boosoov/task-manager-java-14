package com.rencredit.jschool.boruak.taskmanager.entity;

import com.rencredit.jschool.boruak.taskmanager.enumeration.Role;

public class User extends AbstractEntity {

    private String login;

    private String passwordHash;

    private String email;

    private String firstName;

    private String lastName;

    private String middleName;

    private Role role = Role.USER;

    public User() {
    }

    public User(final String login, final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(final String login, final String password, final Role role) {
        this.login = login;
        this.passwordHash = password;
        this.role = role;
    }

    public User(final String login, final String password, final String firstName) {
        this.login = login;
        this.passwordHash = password;
        this.firstName = firstName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(final String middleName) {
        this.middleName = middleName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", role=" + role +
                '}';
    }

}
