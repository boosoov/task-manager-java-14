package com.rencredit.jschool.boruak.taskmanager.exception.busy;

public class BusyLoginException extends RuntimeException {

    public BusyLoginException() {
        super("Error! Login is busy...");
    }

}
