package com.rencredit.jschool.boruak.taskmanager.exception.incorrect;

public class IncorrectHashPasswordException extends RuntimeException {

    public IncorrectHashPasswordException() {
        super("Error! Hash password incorrect...");
    }

}
