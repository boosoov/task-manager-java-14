package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IAuthRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumeration.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyEmailException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyRoleException;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;

import java.util.Objects;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private final IAuthRepository authRepository;

    public AuthService(final IUserService userService, final IAuthRepository authRepository) {
        this.userService = userService;
        this.authRepository = authRepository;
    }

    @Override
    public String getUserId() {
        if (Objects.isNull(authRepository.getUserId())) throw new DeniedAccessException();
        return authRepository.getUserId();
    }

    @Override
    public void logIn(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        final User user = userService.getByLogin(login);
        if (Objects.isNull(user)) throw new UnknownUserException();
        final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new IncorrectHashPasswordException();
        if (!passwordHash.equals(user.getPasswordHash())) throw new IncorrectPasswordException();
        authRepository.logIn(user);
    }

    @Override
    public User registration(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        return userService.add(login, password);
    }

    @Override
    public User registration(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        return userService.add(login, password, email);
    }

    @Override
    public User registration(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null || Objects.isNull(role)) throw new EmptyRoleException();

        return userService.add(login, password, role);
    }

    @Override
    public void logOut() {
        authRepository.logOut();
    }

}
