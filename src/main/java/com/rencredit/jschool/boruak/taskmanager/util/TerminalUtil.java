package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        String str =  SCANNER.nextLine();
        return str;
    }

    static Integer nextNumber() {
        final String line = nextLine();
        try {
            return Integer.parseInt(line);
        } catch (Exception e) {
            throw new IncorrectIndexException(line);
        }
    }

}

//public class TerminalUtil {
//
//    private TerminalUtil() {}
//
//    private static final Scanner SCANNER = new Scanner(System.in);
//
//    public static String nextLine() {
//        return SCANNER.nextLine();
//    }
//
//    public static Integer nextNumber() {
//        final String value = nextLine();
//        try {
//            return Integer.parseInt(value);
//        } catch (Exception e) {
//            throw new IncorrectIndexException(value);
//        }
//    }
//}
